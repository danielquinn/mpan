# Changelog

## 3.0.0

A simple dependencies update, and since I don't work at Limejump (now Shell)
anymore, I figured I should make a copy in case they decide to remove the
original repo one day.  Since this *is* my copy though, I'm changing the
licence from MIT to GPL (hence the major version bump).  Should Shell decide to
relinquish control of the PyPI entry to me, this major version change will keep
anyone from being surprised by the license update.

* Licence change: MIT to GPL
* Removed black, flake8, and isort in favour of [ruff](https://docs.astral.sh/ruff/)
* Updated all the dependencies
* Dropped support for Python 3.8 and 3.9


## 2.1.0

* Allow equality comparison with strings.

## 2.0.0

* We rewrote the distribution portion of the parser to better reflect the n:1
  relationship between GSP groups and distributors.  The `.gsp_group_id`
  (`str`) is gone, replaced with `.gsp_groups` (`list[str]`).
* The API for DNOs and IDNOs are now the same, dropping properties like
  `.licensee` and `mpas_operator_id` in favour of a common `participant_id`.
  See the docs for specifics.


## 1.1.0

* Added support for automatic generation of valid MPANs with either Faker or
  Mimesis.


## 1.0.4

* Minor update to the validation error message.


## 1.0.3

* Bugfix: Comparing two identical MPAN objects now returns boolean `True`,
  while comparing an MPAN object to a string of the same value returns `False`.
* Added lots more documentation to the README.


## 1.0.2

* `.is_valid()` was amended to validate the top row as well.
* `is_valid()` now returns a boolean rather than potentially throwing an
  `InvalidMPANError`.


## 1.0.1

* Minor change to use a new contact email


## 1.0.0

* Initial release
