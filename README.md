![Stop killing Palestinian civilians](https://img.shields.io/badge/It%27s%20Genocide-%F0%9F%87%B5%F0%9F%87%B8%20Tech_For_Palestine-D83838?labelColor=01B861&color=D83838&link=https%3A%2F%2Ftechforpalestine.org%2Flearn-more)

# mpan

[![PyPI](https://img.shields.io/pypi/pyversions/mpan)](https://pypi.org/project/mpan)
[![PyPI](https://img.shields.io/pypi/wheel/mpan)](https://pypi.org/project/mpan)
[![License](https://img.shields.io/pypi/l/mpan)](https://mit-license.org/)
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)
![100% Coverage](https://img.shields.io/badge/coverage-100%25-4ec820.svg)

A library to help you parse the UK energy industry's MPAN number format.


## Links

* [Official documentation](https://danielquinn.gitlab.io/mpan/)
* [Wikipedia article on the MPAN standard](https://en.wikipedia.org/wiki/Meter_Point_Administration_Number)
